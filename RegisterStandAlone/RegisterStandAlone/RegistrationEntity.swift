//
//  RegistrationEntity.swift
//  RegisterStandAlone
//
//  Created by Nakamura, Sho | Sho | BDD on 10/9/14.
//  Copyright (c) 2014 Nakamura, Sho | Sho | BDD. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

class RegistrationEntity: NSObject {

    var image:UIImage?
    var coordinate:CLLocationCoordinate2D?
    var comment:String?
    
    override init() {
        super.init()
    }
 
    
}