//
//  RegisterPinAnnotation.swift
//  RegisterStandAlone
//
//  Created by Nakamura, Sho | Sho | BDD on 10/13/14.
//  Copyright (c) 2014 Nakamura, Sho | Sho | BDD. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

class RegisterPinAnnotation: NSObject, MKAnnotation {
    
    var coordinate:CLLocationCoordinate2D
    var title:String = ""
    var subtitle:String = ""
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String) {
        
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
    
    
}