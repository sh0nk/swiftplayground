//
//  CommentViewController.swift
//  RegisterStandAlone
//
//  Created by Nakamura, Sho | Sho | BDD on 10/14/14.
//  Copyright (c) 2014 Nakamura, Sho | Sho | BDD. All rights reserved.
//

import Foundation
import UIKit

class CommentViewController: UIViewController, UINavigationControllerDelegate {
    
    @IBOutlet weak var commentView: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    
    var registEntity:RegistrationEntity?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        println("image on comment view? ")
        println(self.registEntity!.image)
        
        self.imageView?.image = self.registEntity?.image
        self.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // FIXME
    func sendRequest() {
        self.registEntity?.comment = self.commentView!.text
        println(self.registEntity)
    }
    
    // FIXME: temporary
    @IBAction func tappedSendButton(sender: AnyObject) {
        sendRequest()
        
        var alert = UIAlertController(
            title: "送信しますか？", message: self.registEntity?.comment, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(
            title: "OK", style: UIAlertActionStyle.Default,
            handler: {(action:UIAlertAction!) -> Void in }))
        alert.addAction(UIAlertAction(
            title: "キャンセル", style: UIAlertActionStyle.Cancel,
            handler: {(action:UIAlertAction!) -> Void in }))
        self.presentViewController(alert, animated: true, completion: {})
    }

    

}