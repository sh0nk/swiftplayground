//
//  ViewController.swift
//  RegisterStandAlone
//
//  Created by Nakamura, Sho on 10/6/14.
//  Copyright (c) 2014 Nakamura, Sho. All rights reserved.
//

import UIKit
import AssetsLibrary
import CoreLocation
import ImageIO


class ImageConfirmViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var confirmLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
//    @IBOutlet weak var nextButton: UIButton!
    
    // TODO: modelに入れる
    var registEntity:RegistrationEntity?

    var myCoder:NSCoder
    
    required init(coder aDecoder: NSCoder) {
        self.myCoder = aDecoder
        self.registEntity = RegistrationEntity()
        
        super.init(coder: aDecoder)
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        NSLog("hello Main View loaded")
        
        var imagePicker:UIImagePickerController = UIImagePickerController()
        
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        self.presentViewController(imagePicker, animated: true, completion: nil)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        // modalの初期画面では戻るボタンは表示しない
        // TODO: 戻ってきたとき消えない？？
        self.navigationItem.setHidesBackButton(true, animated: true)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        // 戻るボタン復活
        self.navigationItem.setHidesBackButton(false, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    @IBAction func buttonTapped(sender: AnyObject) {
//        var tappedButton:UIButton = sender as UIButton
//        
//        var mapConfirmVC: MapConfirmViewController = MapConfirmViewController(coder: self.myCoder)
//        mapConfirmVC.setEntity(self.registEntity!)
//        
//        self.navigationController?.pushViewController(mapConfirmVC, animated: true)
//
//    }
    
    
    func getAndSetExif(info: [NSObject : AnyObject]) {
        var lib:ALAssetsLibrary = ALAssetsLibrary()
        var url = info[UIImagePickerControllerReferenceURL] as NSURL
        
        var coordinate: CLLocationCoordinate2D
        lib.assetForURL(url,
            resultBlock: {(asset: ALAsset!) -> Void in
                let location = asset.valueForProperty(ALAssetPropertyLocation) as CLLocation?
                
                if (location != nil) {
                    self.registEntity?.coordinate = location!.coordinate
                    println(self.registEntity?.coordinate)
                    
                    // 次の次の画面（マップ）の準備
//                    var storyboard = UIStoryboard(name: "RegisterMain", bundle: nil)
//                    var mapViewController = storyboard.instantiateViewControllerWithIdentifier("MapConfirmViewController")
//                        as MapConfirmViewController
//                    
//                    mapViewController.setEntity(self.registEntity!)
                } else {
                    NSLog("No location found.")
                }
                
            },
            failureBlock: {(error: NSError!) -> Void in
                NSLog("Error occured when location is being loaded.")
            }
        )
        
    }

    
    // デリゲートメソッド
    func imagePickerController(
        picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
            println(info)
            
            // Exifとる
            getAndSetExif(info)
            
            var image:UIImage
            image = info[UIImagePickerControllerOriginalImage] as UIImage
            self.registEntity?.image = image
            
            println("image: ")
            println(self.registEntity?.image)
            
            // Do any additional setup after loading the view, typically from a nib.
            // imageViewのセット
            self.imageView?.image = self.registEntity?.image
            self.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
            self.view.sendSubviewToBack(self.imageView)
            
            self.confirmLabel.text = "この画像でよろしいですか？"
            println(self.confirmLabel.text)
            
            self.dismissViewControllerAnimated(true, completion: nil)
    }

    // mapViewへの遷移
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "ImageToMap") {
            let mapConfirmViewController = segue.destinationViewController as  MapConfirmViewController
            mapConfirmViewController.registEntity = self.registEntity!
        }
    }
}

