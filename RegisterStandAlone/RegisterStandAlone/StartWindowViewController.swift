//
//  StartWindowViewController.swift
//  RegisterStandAlone
//
//  Created by Nakamura, Sho | Sho | BDD on 10/7/14.
//  Copyright (c) 2014 Nakamura, Sho | Sho | BDD. All rights reserved.
//

import Foundation
import UIKit

class StartWindowViewController: UIViewController {
    
    @IBOutlet var button:UIButton?

    var myCoder:NSCoder

    required init(coder aDecoder: NSCoder) {
        self.myCoder = aDecoder
        
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func buttonTapped(sender:AnyObject) {
        var tappedButton:UIButton = sender as UIButton
        tappedButton.setTitle("tapped", forState:UIControlState.Normal)
        
        // 次画面への遷移
        var storyboard = UIStoryboard(name: "RegisterMain", bundle: nil)
        
        var naviView =
        storyboard.instantiateViewControllerWithIdentifier("RegistrationNavigationController")
            as UINavigationController
        var confirmView = storyboard.instantiateViewControllerWithIdentifier("ImageConfirmViewController")
            as ImageConfirmViewController
        
        naviView.pushViewController(confirmView, animated: true)
        self.presentViewController(naviView, animated: true, completion: nil)

    }

    

}