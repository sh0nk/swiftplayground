//
//  MapConfirmViewController.swift
//  RegisterStandAlone
//
//  Created by Nakamura, Sho | Sho | BDD on 10/9/14.
//  Copyright (c) 2014 Nakamura, Sho | Sho | BDD. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

// Sample image: http://www.opanda.com/cn/iexif/images/gg_gps.jpg

class MapConfirmViewController: UIViewController, MKMapViewDelegate {
        
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var confirmMapLabel: UILabel!
    
    // TODO: modelに入れる
    var registEntity:RegistrationEntity?
    var uniqueAnnotaton:MKAnnotation?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        if (self.confirmMapLabel == nil) {
            self.confirmMapLabel = UILabel()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        NSLog("hello Map View loaded")
        if (self.mapView == nil) {
            self.mapView = MKMapView()
//            self.mapView = MKMapView(frame: CGRectMake(0, 180, self.view.frame.width, self.view.frame.height - 180))
        }
        self.mapView.delegate = self
        
        println("coord called: ")
        println(self.registEntity!.coordinate?.longitude)
        
        // Initialize Gesture Recognizer
        var recognizer:UILongPressGestureRecognizer
            = UILongPressGestureRecognizer(target: self, action: "notifiedLongPress:")
        self.mapView.addGestureRecognizer(recognizer)
        
        self.view.addSubview(self.mapView)
        //self.view.bringSubviewToFront(self.confirmMapLabel)

        confirmMapLabel.adjustsFontSizeToFitWidth = true

        if (self.registEntity!.coordinate != nil) {
            let coordinate = self.registEntity!.coordinate!
            let region = MKCoordinateRegionMakeWithDistance(coordinate, 1000, 1000)
            mapView.setRegion(region, animated: true)
            mapView.setCenterCoordinate(coordinate, animated: true)
            
            // Annotation
            if (self.uniqueAnnotaton == nil) {
                setUniqueAnnotation(coordinate)
            }
            
            confirmMapLabel.text = "この場所でよろしいですか？\n調整したい場合は、長押しでピンを追加して下さい。"
        } else {
            confirmMapLabel.text = "この写真を撮影した場所に、長押しでピンを追加して下さい。"
        }

    }
    
    
    func notifiedLongPress(recognizer:UILongPressGestureRecognizer) {
        if (recognizer.state == UIGestureRecognizerState.Began) {
            let point = recognizer.locationInView(self.mapView)
            let coordinate = self.mapView.convertPoint(point, toCoordinateFromView: self.mapView)
            
            setUniqueAnnotation(coordinate)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setUniqueAnnotation(coordinate: CLLocationCoordinate2D) {
        var annotation = RegisterPinAnnotation(coordinate: coordinate,
            title: "写真の位置(googleマップではLocationInfoも)", subtitle: String(
                format: "経度: %.2f, 緯度: %.2f", arguments: [coordinate.longitude, coordinate.latitude]))
        
        self.uniqueAnnotaton = annotation
        println("annotations: ", self.mapView.annotations.count)
        if (self.mapView.annotations.count > 0) {
            self.mapView.removeAnnotations(self.mapView.annotations)
        }
        self.mapView.addAnnotation(self.uniqueAnnotaton)
        
        
    }
    
    // mapViewへの遷移の判断
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if (identifier == "MapToComment") {
            if (self.uniqueAnnotaton == nil) {
                var alert = UIAlertController(
                    title: "", message: "撮影場所を追加して下さい。", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(
                    title: "OK", style: UIAlertActionStyle.Default,
                    handler: {(action:UIAlertAction!) -> Void in }))
                self.presentViewController(alert, animated: true, completion: {})
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }
    
    // mapViewへの遷移時
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "MapToComment") {
            let commentConfirmViewController = segue.destinationViewController as CommentViewController
            commentConfirmViewController.registEntity = self.registEntity!
        }
    }


}